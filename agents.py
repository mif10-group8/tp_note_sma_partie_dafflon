import pygame

import core
from pygame.math import Vector2
import random
from body import *

class Agents:
    def __init__(self, id):
        self.id = id
        self.body = Body()


    def show(self):
        if self.body.status == 0:
            core.Draw.circle((255, 255, 255), self.body.pos, self.body.size)
        elif self.body.status == 1:
            core.Draw.circle((255, 0, 0), self.body.pos, self.body.size)
        elif self.body.status == 2:
            core.Draw.circle((0, 255, 0), self.body.pos, self.body.size)
        elif self.body.status == 3:
            core.Draw.circle((0, 0, 0), self.body.pos, self.body.size)

    def DeplacementAleatoire(self):
        acc = Vector2(random.randint(-1, 1), random.randint(-1, 1))
        self.body.acceleration = acc

    def DeplacementRationnel(self):
        for boids in self.body.circularFrustrum.ElemInVision:
            if boids.body.status == 0:
                acc = boids.body.pos + self.body.pos
                self.body.acceleration = acc














