import core
from pygame.math import Vector2
import random


class circularFrustrum:
    def __init__(self, pos, size, size_body):
        self.pos = pos
        self.size = size
        self.ElemInVision = []
        self.size_body = size_body

    def isInside(self, Object):

        if self.pos.distance_to(Object.pos) <= self.size + Object.size + self.size_body:
            return True
        else:
            return False
