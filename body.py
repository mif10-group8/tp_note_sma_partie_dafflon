import core
from pygame.math import Vector2
import random

from frustrum import *
from epidemie import *

class Body:
    def __init__(self):
        self.pos = Vector2(random.randint(0, 800), random.randint(0, 600))
        self.size = 10
        self.velocity = Vector2(0, 0)
        self.acceleration = Vector2(0, 0)
        self.maxSpeed = 5
        self.maxAcceleration = 0.5
        self.sizeFrustrum = 20
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.circularFrustrum = circularFrustrum(self.pos, self.sizeFrustrum, self.size)

        self.status = 0

        self.duree_incubation = 0
        self.duree_avant_contagion = 0
        self.pourcentage_contagion = epidemie1.pourcentage_contagion
        self.duree_avant_deces = 0
        self.pourcentage_mortalite = epidemie1.pourcentage_mortalite
        self.Distance_Mini_de_contagion = epidemie1.Distance_Mini_de_contagion

        self.maladeRationnel = random.random()

    def move(self):
        if self.acceleration.length() > self.maxAcceleration:
            self.acceleration.scale_to_length(self.maxAcceleration)
        self.velocity += self.acceleration
        if self.velocity.length() > self.maxSpeed:
            self.velocity.scale_to_length(self.maxSpeed)
        self.pos += self.velocity




    #Prends en comptes les paramètres d'une epidemie
    def uptade(self):
        if self.status == 1:
            if self.duree_avant_contagion > epidemie1.duree_avant_contagion:
                for boids in self.circularFrustrum.ElemInVision:
                   if self.Distance_Mini_de_contagion < self.pos.distance_to(boids.body.pos):
                    if boids.body.status == 0:
                        rand = random.random()
                        if rand < self.pourcentage_contagion:
                            print(rand, "rand", self.pourcentage_contagion, "pourcentage_contagion")
                            boids.body.status = 1
                            boids.body.duree_incubation = 0
                            boids.body.duree_avant_contagion = 0
                            boids.body.pourcentage_contagion = epidemie1.pourcentage_contagion
                            boids.body.duree_avant_deces = 0
                            boids.body.pourcentage_mortalite = epidemie1.pourcentage_mortalite
                            boids.body.Distance_Mini_de_contagion = epidemie1.Distance_Mini_de_contagion

            else:
                self.duree_avant_deces += 1
            if self.duree_avant_deces < epidemie1.duree_avant_deces:
                self.duree_avant_deces += 1
            else:
                if random.random() < self.pourcentage_mortalite:
                    self.status = 3
                else:
                    self.status = 2
            self.duree_avant_contagion += 1













