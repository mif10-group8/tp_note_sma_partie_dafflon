import pygame
import core
from agents import *


def setup():
 core.fps = 20
 core.WINDOW_SIZE = [800, 600]
 core.memory("agents", [])
 core.memory("materiaux", [])
 Addall()



def computePerception():
    for boid in core.memory("agents"):
        boid.body.circularFrustrum.ElemInVision = []

    for boid in core.memory("agents"):
        for boid2 in core.memory("agents"):
            if boid.body.circularFrustrum.isInside(boid2.body) and boid != boid2:
                boid.body.circularFrustrum.ElemInVision.append(boid2)
                boid.body.circularFrustrum.ElemInVision.append(boid2)


def computeDecision():
    for boid in core.memory("agents"):
        if boid.body.maladeRationnel > 0.3 and boid.body.status == 1:
            boid.DeplacementRationnel()
        else:
            boid.DeplacementAleatoire()

def applyDecision():

    for boid in core.memory("agents"):
        boid.body.move()
        boid.body.uptade()



def teleportation():
    for boid in core.memory("agents"):
        if boid.body.pos.x > 800:
            boid.body.pos.x = 0
        if boid.body.pos.x < 0:
            boid.body.pos.x = 800
        if boid.body.pos.y > 600:
            boid.body.pos.y = 0
        if boid.body.pos.y < 0:
            boid.body.pos.y = 600
def AffichageS_I_R_D():
    S = 0
    I = 0
    R = 0
    D = 0
    for boid in core.memory("agents"):
        if boid.body.status == 0:
            S += 1
        elif boid.body.status == 1:
            I += 1
        elif boid.body.status == 2:
            R += 1
        elif boid.body.status == 3:
            D += 1
    core.Draw.text((255,255,255),"Sains : " + str(S), (0, 0))
    core.Draw.text((255,255,255),"Infectés : " + str(I), (0, 20))
    core.Draw.text((255,255,255),"Rétablis : " + str(R), (0, 40))
    core.Draw.text((255,255,255),"Décédés : " + str(D), (0, 60))



def draw():

    for agents in core.memory("agents"):
        agents.show()
        core.Draw.circle((255, 255, 255), agents.body.circularFrustrum.pos, agents.body.circularFrustrum.size, 1)

# Change le status de l'agent sur lequel on clique
def mousePressed():
    if pygame.MOUSEBUTTONUP:
        pos = pygame.mouse.get_pos()
        for boid in core.memory("agents"):
            if boid.body.pos.distance_to(pos) < boid.body.size:
                boid.body.status = 1


def Addall():

    for i in range(0, 20):
        core.memory("agents").append(Agents(i))


def game():
 draw()
 computePerception()
 computeDecision()
 applyDecision()
 teleportation()
 mousePressed()
 AffichageS_I_R_D()

def run():
 core.cleanScreen()
 game()

 if pygame.key.get_pressed()[pygame.K_p]:
  core.memory("agents").append(Agents(core.memory("agents").__len__()))
 if pygame.key.get_pressed()[pygame.K_SPACE]:
  Addall()
 if pygame.key.get_pressed()[pygame.K_r]:
  setup()

core.main(setup, run)






